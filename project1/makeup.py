'''
Created on 2012-2-28

@author: admin
'''

import sys, re
from handlers import *
from util import *
from rules import *

class Parser:
    
    def __init__(self, handler):
        self.handler = handler
        self.rules = []
        self.filters = []
    
    def addRule(self, rule):
        self.rules.append(rule)
        
    def addFilter(self, pattern, name):
        def filter(block, handler):
            return re.sub(pattern, handler.sub(name), block)
        self.filters.append(filter)
        
        
    def parse(self, file):
        self.handler.start('document')
        
        """
        map the filter to the suitable string in each block
        and find the block belong to which rule
        """
        for block in blocks(file):
            for filter in self.filters:
                block = filter(block, self.handler)
            for rule in self.rules:
                if rule.condition(block):
                    last = rule.action(block, self.handler)
                    if last: break;
        self.handler.end('document')
        

class BasicTextParser(Parser):
    def __init__(self, handler):
        Parser.__init__(self, handler)
        
        """
        add rule, the rule's seq can not be random
        because parser.parse() use for to find suit rule, the Rule realize is not independence
        """
        self.addRule(ListRule())
        self.addRule(ListItemRule())
        self.addRule(TitleRule())
        self.addRule(HeadingRule())
        self.addRule(ParagraphRule())
        
        self.addFilter(r'\*(.+?)\*', 'emphasis')
        self.addFilter(r'(http://[\.a-zA-Z0-9/]+)', 'url')
        self.addFilter(r'([\.a-zA-Z0-9]+@[\.a-zA-Z0-9]+[a-zA-Z]+)', 'mail')



sys.stdout = open('output.html', 'w')
handler = HTMLRenderer()
parser = BasicTextParser(handler)

parser.parse(open('../input.txt'))
       
                  
        
