'''
Created on 2012-2-28

@author: admin
'''

def lines(file):
    for line in file: yield line
    yield '\n'
    
def blocks(file):
    block = []
    for line in lines(file):
        if line.strip():
            block.append(line);
        elif block:
            yield ''.join(block).strip()
            block = []
         
