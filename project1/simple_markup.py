'''
Created on 2012-2-28

@author: Hokein
'''

import sys, re, util

sys.stdin = open('input.txt')
sys.stdout = open('output.html', 'w')

print '<html><head><title>...</title><body>'

title = True

for block in util.blocks(sys.stdin):
    block = re.sub(r'\*(.+?)\*', r'<em>\1</em>', block)
    
    if title:
        print '<h1>' + block + '</h1>'
        title = False
    else:
        print '<p>' + block + '</p>'
        
print '</body></html>'
